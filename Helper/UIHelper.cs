﻿// -----------------------------------------------------------------------
// <copyright file="UIHelper.cs" company="Ideo">
// Copyright (c) Ideo 2021 All Rights Reserved
// </copyright>
// <author>Maarten Brands</author>
// <date>24-09-2021</date>
// ---
namespace Ideo.Andel.ClickMobile.Helper
{
    using System;
    using System.Html;
    using System.Serialization;
    using Ideo.Andel.ClickMobile.NetFiles.Helper;
    using Ideo.Andel.ClickMobile.NetFiles.Model;
    using jQueryApi;
    using W6.ClickMobile.Web.Core;
    using W6.ClickMobile.Web.ObjectModel;

    /// <summary>
    /// Helper to build HTML containers
    /// </summary>
    public class UIHelper
    {
        /// <summary>
        /// Build up the HTML container to show empty message
        /// </summary>
        /// <param name="isEmpty">Does the container be shown or not</param>
        /// <param name="text">Empty message to be shown to he user</param>
        /// <returns>HTML Container</returns>
        public static jQueryObject CreateEmptyMessageElement(bool isEmpty, string text)
        {
            return jQuery
                    .FromHtml("<div>")
                    .Attribute("id", "emptyLabel")
                    .AddClass("emptyMessageLabel")
                    .CSS("display", isEmpty ? "block" : "none")
                    .Text(text);
        }

        /// <summary>
        /// Build NetFiles HTML table
        /// </summary>
        /// <param name="list">List of files from NetFiles</param>
        /// <param name="propertyNameNetFilesId">Property name of the File ID</param>
        /// <returns>HTML table</returns>
        public static jQueryObject CreateItemsContainer(W6CMMultiValueOfLinkedBusinessObject list, string propertyNameNetFilesId)
        {
            if (list.Count == 0)
            {
                return null;
            }

            jQueryObject main = jQuery.FromHtml("<div>");

            jQueryObject itemsContainer = jQuery
                                            .FromHtml("<div>")
                                            .Attribute("id", "itemsContainer")
                                            .AddClass("W6ListView");

            main.Append(jQuery
                            .FromHtml("<div>")
                            .Attribute("id", "myModal")
                            .AddClass("modal")
                            .Append(
                                jQuery
                                    .FromHtml("<span>")
                                    .AddClass("close")
                                    .Text("x")
                                    .Click(
                                        delegate(jQueryEvent e)
                                        {
                                            // When the user clicks on <span> (x), close the modal
                                            Document.GetElementById("myModal").Style.Display = "none";
                                        }))
                            .Append(
                                jQuery
                                    .FromHtml("<img>")
                                    .Attribute("id", "img01")
                                    .AddClass("modal-content"))
                            .Append(
                                jQuery
                                    .FromHtml("<div>")
                                    .Attribute("id", "caption")));

            jQueryObject itemMainElement = jQuery
                                            .FromHtml("<div>")
                                            .AddClass("W6AttachmentsList")
                                            .AddClass("itemMainElement")
                                            .CSS("position", "relative");

            TableElement element = (TableElement)Document.CreateElement("table");
            element.ID = "itemTable";
            element.ClassName = "highlightEnable";

            foreach (W6CMLinkedBusinessObjectItem item in list)
            {
                string fileId = NetFilesHelper.GetFileIdFromURL(item.DisplayString);

                if (fileId != null)
                {
                    NetFile netFile = Json.ParseData<NetFile>(W6CMLocalStorageFactory.GetInstance().getItem(string.Format(NetFilesHelper.LocalFileStorageMetadata, item.Key) + fileId));

                    if (netFile != null)
                    {
                        AddItem(element, netFile);
                    }
                }
            }

            itemMainElement
                .Append(element)
                .AppendTo(itemsContainer);
            main.Append(itemsContainer);

            return main;
        }

        /// <summary>
        /// Add item in the Attachment list
        /// </summary>
        /// <param name="element">Table HTML element</param>
        /// <param name="netFile">NetFile attachment object</param>
        public static void AddItem(TableElement element, NetFile netFile)
        {
            if (!netFile.ContentType.StartsWith("image"))
            {
                return;
            }

            TableRowElement fileRow = element.InsertRow();

            fileRow.AddEventListener(
                "click",
                delegate(ElementEvent e)
                {
                    Script.Literal("console.log('Open file ' + {0} + ' of type ' + {1})", netFile.Id, netFile.ContentType);
                    Script.Literal(
                        @"Ideo.Andel.ClickMobile.CustomScripts.IndexDB.GetFile({0}).then(function(result){{",
                        NetFilesHelper.LocalFileStorageContent + netFile.Id);

                    jQuery.Select("#listViewItemCheckBox_" + (string)Script.Literal("result.Key")).Attribute("src", string.Format("data:{0};base64,{1}", netFile.ContentType, (string)Script.Literal("result.Content")));

                    if (!W6CMPlatform.Is_Cordova_V2() && netFile.ContentType == "image/jpeg")
                    {
                        Document.GetElementById("myModal").Style.Display = "block";
                        ((ImageElement)Document.GetElementById("img01")).Src = string.Format("data:{0};base64,{1}", netFile.ContentType, (string)Script.Literal("result.Content"));
                        Document.GetElementById("caption").InnerHTML = netFile.Name;
                    }
                    else
                    {
                        try
                        {
                            W6CMAttachmentsFile file = new W6CMAttachmentsFile();

                            file.WriteFileData(
                                NetFilesHelper.LocalFileStorageContent + netFile.Id + "_" + netFile.Name, 
                                file.base64ToBinary((string)Script.Literal("result.Content")), 
                                delegate(string filePath)
                                {
                                    Script.Literal("console.log('Open file at filepath: ' + {0})", filePath);
                                    filePath = W6CMPhoneGapProxy.GetWorkingDirectory().ToURL() + filePath;
                                    Script.Literal("cordova.plugins.fileOpener2.open({0}, {1})", filePath, netFile.ContentType);
                                },
                                delegate(object error) 
                                {
                                    Script.Literal("console.error('Error occured writing file')");
                                    Script.Literal("console.error({0})", error);
                                });
                        }
                        catch (Exception ex)
                        {   
                            Script.Alert("Error while opening file, please contact your administrator. Details: " + ex.Message);
                            Script.Literal("console.error('Native App Launcher error! Unable to open the requested URL.')");
                            Script.Literal("console.error({0})", ex);
                        }
                    }

                    Script.Literal("console.log(result);})");
                },
                false);

            TableCellElement itemCheckboxCell = fileRow.InsertCell();
            itemCheckboxCell.ID = "itemCheckboxCell";

            ImageElement listViewItemCheckBox = (ImageElement)Document.CreateElement("img");
            listViewItemCheckBox.ID = "listViewItemCheckBox_" + NetFilesHelper.LocalFileStorageThumbnail + netFile.Id;
            listViewItemCheckBox.ClassName = "W6ListViewItemCheckBox";
            
            Script.Literal(@"Ideo.Andel.ClickMobile.CustomScripts.IndexDB.GetFile({0}).then(function(result){{", NetFilesHelper.LocalFileStorageThumbnail + netFile.Id);
            
            jQuery.Select("#listViewItemCheckBox_" + (string)Script.Literal("result.Key")).Attribute("src", string.Format("data:image/jpeg;base64,{0}", (string)Script.Literal("result.Content")));
            
            Script.Literal("console.log(result);})");

            jQuery
                .FromHtml("<div>")
                .Attribute("id", "itemCheckBox")
                .Append(listViewItemCheckBox)
                .AppendTo(itemCheckboxCell);

            TableCellElement itemTextCell = fileRow.InsertCell();
            itemTextCell.ID = "itemTextCell";

            jQuery
                .FromHtml("<div>")
                .Attribute("id", "titleText")
                .Text(netFile.Name)
                .AppendTo(itemTextCell);

            jQuery
                .FromHtml("<div>")
                .Attribute("id", "secondaryText")
                .Text(string.Empty)
                .AppendTo(itemTextCell);

            jQuery
                .FromHtml("<div>")
                .Attribute("id", "itemText")
                .Text(string.Empty)
                .AppendTo(itemTextCell);

            TableCellElement itemMarkCell = fileRow.InsertCell();
            itemMarkCell.ID = "itemMarkCell";
            itemMarkCell.Style.Width = "150px";
        }
    }
}
