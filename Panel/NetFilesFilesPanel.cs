﻿// -----------------------------------------------------------------------
// <copyright file="NetFilesFilesPanel.cs" company="Ideo">
// Copyright (c) Ideo 2021 All Rights Reserved
// </copyright>
// <author>Maarten Brands</author>
// <date>25-09-2021</date>
// ---
namespace Ideo.Andel.ClickMobile.Panels
{
    using System;
    using System.Collections.Generic;
    using System.Html;
    using System.Serialization;
    using Ideo.Andel.ClickMobile.Helper;
    using Ideo.Andel.ClickMobile.Settings;
    using jQueryApi;
    using W6.ClickMobile.Web.ObjectModel;
    using W6.ClickMobile.Web.UI.Controls.PropertyPanels;

    /// <summary>
    /// This custom panel is showing a table of NetFiles and the opportunity to take or add pictures.
    /// </summary>
    public class NetFilesFilesPanel : W6CMCustomPanel
    {
        /// <summary>
        /// Settings of the panel set via mobility studio
        /// </summary>
        private FilePanelSettings settings = null;

        /// <summary>
        /// List of photos which are added to the list and needs to be commit
        /// </summary>
        private List<W6CMGenericObject> lstOfPhotosToCommit = null;

        /// <summary>
        /// Initialize the custom panel
        /// </summary>
        public override void Init()
        {
            try
            {
                base.Init();
                this.lstOfPhotosToCommit = new List<W6CMGenericObject>();
                _objLog.Debug(string.Format("Load NetFilesFilesPanel with settings: ", this.CustomSettings));
                this.settings = Json.ParseData<FilePanelSettings>(this.CustomSettings);

                jQueryObject header = jQuery.FromHtml("<div>")
                    .AddClass("W6CMPanel")
                    .AddClass("W6CMPropPanelMultiValueOfAttachments").AppendTo(MainElement);

                TableElement element = (TableElement)Document.CreateElement("table");
                element.Style.Width = "100%";
                TableRowElement rowLabel = element.InsertRow();
                TableCellElement cellLabel = rowLabel.InsertCell();
                cellLabel.ID = "lableContainer";
                cellLabel.AppendChild(
                    jQuery.FromHtml("<div>")
                    .AddClass("W6CMPropPanel_LabelPanel")
                    .Append(
                        jQuery
                            .FromHtml("<div>")
                            .Attribute("objname", "W6CMLabelControl")
                            .Append(jQuery
                                .FromHtml("<div>")
                                .Attribute("id", "Text")
                                .CSS("overflow", "hidden")
                                .CSS("word-break", "break-word")
                                .CSS("display", "inline")
                                .Text(this.settings.Label))).GetElement(0));

                W6CMMultiValueOfLinkedBusinessObject list = (W6CMMultiValueOfLinkedBusinessObject)this.FormObject.GetPropertyByName((int)W6CMObjectTypes.Task, this.settings.MultiValuePropertyNameNetFiles);
                rowLabel = element.InsertRow();
                TableCellElement cellValue = rowLabel.InsertCell();
                cellValue.ID = "valueContainer";
                cellValue.AppendChild(
                    jQuery.FromHtml("<div>")
                        .Attribute("objname", "ValuePanel")
                        .AddClass("W6CMPropPanel_ValuePanel")
                        .Append(
                            jQuery
                                .FromHtml("<div>")
                                .AddClass("W6CMPropPanel_ValuePanel")
                                .Append(UIHelper.CreateEmptyMessageElement(list.Count == 0, this.settings.EmptyMessage))
                                .Append(UIHelper.CreateItemsContainer(list, this.settings.PropertyNameNetFilesId))).GetElement(0));
                
                header.Append(element);
            }
            catch (Exception ex)
            {
                this.ControllerServices.ShowMessageBox(string.Format("An error occured, please contact your administrator. Error Message: {0}", ex.Message), "Error", W6CMAdaptiveViewWindowSelectButtonsType.Ok, null);
                this._objLog.ErrorEx(ex.Message, ex);
            }
        }
    }
}
