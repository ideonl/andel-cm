﻿// -----------------------------------------------------------------------
// <copyright file="FilePanelSettings.cs" company="Ideo">
// Copyright (c) Ideo 2021 All Rights Reserved
// </copyright>
// <author>Maarten Brands</author>
// <date>25-09-2021</date>
// ---
namespace Ideo.Andel.ClickMobile.Settings
{
    #region Using

    using System.Runtime.CompilerServices;
    using W6.ClickMobile.Web.Core;

    #endregion

    /// <summary>
    /// Settings for the NetFiles Files Panel set via the mobility studio
    /// </summary>
    public class FilePanelSettings
    {
        /// <summary>
        /// Property Id of the multi-value which contains the NetFiles
        /// </summary>
        [PreserveCase]
        public int BusinessObjectIdNetFiles;

        /// <summary>
        /// Property name of the multi-value which contains the NetFiles
        /// </summary>
        [PreserveCase]
        public string BusinessObjectNameNetFiles;

        /// <summary>
        /// Should the Button HTML panel been shown
        /// </summary>
        [PreserveCase]
        public bool ButtonAreaVisible = true;

        /// <summary>
        /// Empty message to be shown to the users when the NetFiles table is empty
        /// </summary>
        [PreserveCase]
        public string EmptyMessage;

        /// <summary>
        /// Encoding type of the picture made by the camera
        /// </summary>
        [PreserveCase]
        public CameraEncodingType EncodingType;

        /// <summary>
        /// Label shown at the panel
        /// </summary>
        [PreserveCase]
        public string Label;

        /// <summary>
        /// Property name of the multi-value which contains the NetFiles
        /// </summary>
        [PreserveCase]
        public string MultiValuePropertyNameNetFiles;

        /// <summary>
        /// Quality of the photos taken by the camera, scale 0 to 100.
        /// </summary>
        [PreserveCase]
        public int PhotoQuality;

        /// <summary>
        /// Property name of the File ID of the NetFile in the multi-value
        /// </summary>
        [PreserveCase]
        public string PropertyNameNetFilesId;
    }
}