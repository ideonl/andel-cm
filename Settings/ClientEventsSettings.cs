﻿// -----------------------------------------------------------------------
// <copyright file="ClientEventsSettings.cs" company="Ideo">
// Copyright (c) Ideo 2021 All Rights Reserved
// </copyright>
// <author>Maarten Brands</author>
// <date>17-09-2021</date>
// ---
namespace Ideo.Andel.ClickMobile.Settings
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Settings for the Clients Events set via Mobility Studio
    /// </summary>
    public class ClientEventsSettings
    {
        /// <summary>
        /// The Azure Client ID to authorize the user
        /// </summary>
        [PreserveCase]
        public string ClientId;

        /// <summary>
        /// NetFiles Base URL to build up the URL for the NetFile
        /// </summary>
        [PreserveCase]
        public string NetFilesBaseURL;

        /// <summary>
        /// Property name of multi-value which contains the NetFiles
        /// </summary>
        [PreserveCase]
        public string MultiValuePropertyNameNetFiles;

        /// <summary>
        /// NetFiles File ID property name in the multi-value
        /// </summary>
        [PreserveCase]
        public string PropertyNameNetFilesId;

        /// <summary>
        /// RedirectURI to authorize against Azure
        /// </summary>
        [PreserveCase]
        public string ServiceId;
    }
}