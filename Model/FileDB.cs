﻿// -----------------------------------------------------------------------
// <copyright file="FileDB.cs" company="Ideo">
// Copyright (c) Ideo 2021 All Rights Reserved
// </copyright>
// <author>Maarten Brands</author>
// <date>25-09-2021</date>
// ---
namespace Ideo.Andel.ClickMobile.Model
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// File object representation from IndexedDB
    /// </summary>
    public class FileDB
    {
        /// <summary>
        /// Key record
        /// </summary>
        [PreserveCase]
        public string Key;

        /// <summary>
        /// Form object reference
        /// </summary>
        [PreserveCase]
        public int Reference;

        /// <summary>
        /// Base64 file content
        /// </summary>
        [PreserveCase]
        public string Content;
    }
}
