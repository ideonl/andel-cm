﻿// -----------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="Ideo">
// Copyright (c) Ideo 2021 All Rights Reserved
// </copyright>
// <author>Maarten Brands</author>
// <date>25-09-2021</date>
// ---
using System.Reflection;
using System.Runtime.CompilerServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Click FSE ClickMobile Add-ins for Andel")]
[assembly: AssemblyDescription("Click FSE ClickMobile Add-ins for Andel")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Ideo")]
[assembly: AssemblyProduct("Click FSE ClickMobile")]
[assembly: AssemblyCopyright("Copyright © Ideo 2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ScriptAssembly("ClickMobileAddins")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1")]
[assembly: AssemblyFileVersion("1")]
