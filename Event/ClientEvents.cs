﻿// -----------------------------------------------------------------------
// <copyright file="ClientEvents.cs" company="Ideo">
// Copyright (c) Ideo 2021 All Rights Reserved
// </copyright>
// <author>Maarten Brands</author>
// <date>25-09-2021</date>
// ---
namespace Ideo.Andel.ClickMobile.Event
{
    using System;
    using System.Html;
    using System.Serialization;
    using Ideo.Andel.ClickMobile.NetFiles;
    using Ideo.Andel.ClickMobile.NetFiles.Helper;
    using Ideo.Andel.ClickMobile.Settings;
    using W6.ClickMobile.Web.AddIns.AddInManager;
    using W6.ClickMobile.Web.AddIns.ClientServices;
    using W6.ClickMobile.Web.AddIns.EventArguments;
    using W6.ClickMobile.Web.ObjectModel;

    /// <summary>
    /// Events of the CLickMobile client
    /// </summary>
    public class ClientEvents
    {
        /// <summary>
        /// SXP Message to retrieve the client secret from the administration tool
        /// </summary>
        private const string SxpGetClientSecret = "<SXPServerGetSettings><Setting><Owner>[Application]</Owner><Category>Custom Components</Category><SubCategory>ClientSecret</SubCategory></Setting><RequestedProperties><Item>Body</Item></RequestedProperties></SXPServerGetSettings>";

        /// <summary>
        /// Settings for the client events set via mobility studio
        /// </summary>
        private ClientEventsSettings settings = null;

        /// <summary>
        /// ClickMobile application services
        /// </summary>
        private IW6CMControllerServices controllerServices = null;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ClientEvents"/> class.
        /// </summary>
        /// <param name="settings">Settings set via mobility studio</param>
        public ClientEvents(string settings)
        {
            this.settings = Json.ParseData<ClientEventsSettings>(settings);
            W6CMAddInManager.ClientEvents.AfterClientInitialized += this.ClientEvents_AfterClientInitialized;
            W6CMAddInManager.ClientEvents.AfterScheduleReceivedAndProcessed += this.ClientEvents_AfterScheduleReceivedAndProcessed;
            W6CMAddInManager.ClientEvents.WrappedCustomSXPResultReceived += this.ClientEvents_WrappedCustomSXPResultReceived;
        }

        /// <summary>
        /// Event handler for handling incoming custom SXP messages
        /// </summary>
        /// <param name="sender">Application services</param>
        /// <param name="e">Event arguments</param>
        private void ClientEvents_WrappedCustomSXPResultReceived(object sender, W6CMWrappedCustomSXPResultReceivedEventArgs e)
        {
            if (e.MessageName == "SXPServerGetSettingsResult" && e.Message.SelectSingleNode("Body") != null)
            {
                string clientSecret = e.Message.SelectSingleNode("Body").InnerText;
                
                NetFilesClient.GetAccessToken(
                    this.settings.NetFilesBaseURL,
                    this.settings.ServiceId,
                    this.settings.ClientId,
                    clientSecret, 
                    delegate(string accesstoken)
                    {
                        if (accesstoken != null)
                        {
                            try
                            {
                                (sender as IW6CMControllerServices).GetAssignments(
                                    delegate(W6CMObjectsList assignmentList)
                                    {
                                        foreach (W6CMAssignment assignment in assignmentList)
                                        {
                                            NetFilesHelper.LoadFilesToLocalStorage(
                                                sender as IW6CMControllerServices,
                                                this.settings.MultiValuePropertyNameNetFiles,
                                                assignment,
                                                this.settings.PropertyNameNetFilesId,
                                                accesstoken);
                                        }
                                    });
                            }
                            catch (Exception ex)
                            {
                                Script.Alert(ex.Message);
                                Script.Literal("console.error({0})", ex);
                            }
                        }
                        else
                        {
                            this.controllerServices.DebugLog(string.Format("Access token cannot be fetched"));
                            Script.Literal("console.log('Access token cannot be fetched')");
                        }
                    });
            }
        }

        /// <summary>
        /// Event handler for after the client is initialized
        /// </summary>
        /// <param name="controllerServices">Application services</param>
        /// <param name="e">Event arguments</param>
        private void ClientEvents_AfterClientInitialized(IW6CMControllerServices controllerServices, W6CMAfterClientInitializedEventArgs e)
        {
            this.controllerServices = controllerServices;

            // For initial loading start loading the files later, otherwise with a new installation it doesn't have enough time to load assignments.
            Window.SetTimeout(
                delegate
                {
                    controllerServices.SendWrappedCustomSXP(SxpGetClientSecret);
                }, 
                10000);
        }

        /// <summary>
        /// Event handler after the schedule is received and processed
        /// </summary>
        /// <param name="sender">Object which triggered the event</param>
        /// <param name="e">Event arguments</param>
        private void ClientEvents_AfterScheduleReceivedAndProcessed(object sender, W6CMAfterScheduleReceivedEventArgs e)
        {
            if (e.AddedAssignments != null)
            {
                this.controllerServices.SendWrappedCustomSXP(SxpGetClientSecret);
            }

            if (e.UpdatedAssignments != null)
            {
                this.controllerServices.SendWrappedCustomSXP(SxpGetClientSecret);
            }

            if (e.DeletedAssignments != null)
            {
                foreach (W6CMObject assignment in e.DeletedAssignments)
                {
                    NetFilesHelper.DeleteFilesFromLocalStorage(assignment, this.settings.MultiValuePropertyNameNetFiles);
                }
            }
        }
    }
}
