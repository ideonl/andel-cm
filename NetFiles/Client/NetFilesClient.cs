﻿// -----------------------------------------------------------------------
// <copyright file="NetFilesClient.cs" company="Ideo">
// Copyright (c) Ideo 2021 All Rights Reserved
// </copyright>
// <author>Maarten Brands</author>
// <date>25-09-2021</date>
// ---
namespace Ideo.Andel.ClickMobile.NetFiles
{
    using System;
    using Ideo.Andel.ClickMobile.NetFiles.Model;
    using W6.ClickMobile.Web.AddIns.ClientServices;

    /// <summary>
    /// Delegate of retrieval of the content/thumbnail file
    /// </summary>
    /// <param name="fileId">File ID of the file in NetFiles</param>
    /// <param name="base64">Base64 representation of the file</param>
    public delegate void GetFileBase64Callback(string fileId, string base64);

    /// <summary>
    /// Callback function after the meta data is retrieved from NetFiles
    /// </summary>
    /// <param name="netfile">NetFile attachment object</param>
    public delegate void GetMetadataFileCallback(NetFile netfile);

    /// <summary>
    /// Callback function after uploading the attachment to NetFiles
    /// </summary>
    /// <param name="netFile">NetFile attachment object</param>
    public delegate void UploadFileCallback(NetFile netFile);

    /// <summary>
    /// Callback function after receiving the access token
    /// </summary>
    /// <param name="accesstoken">The access token</param>
    public delegate void GetAccessTokenCallback(string accesstoken);

    /// <summary>
    /// Client to access the NetFiles files
    /// </summary>
    public class NetFilesClient
    {
        /// <summary>
        /// Singleton instance member
        /// </summary>
        private string accessToken = null;

        /// <summary>
        /// Application services for logging
        /// </summary>
        private IW6CMControllerServices controllerServices = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="NetFilesClient"/> class.
        /// </summary>
        /// <param name="controllerServices">Application services</param>
        /// <param name="accessToken">Access token for NetFiles</param>
        public NetFilesClient(IW6CMControllerServices controllerServices, string accessToken)
        {
            this.controllerServices = controllerServices;
            this.accessToken = accessToken;
        }

        /// <summary>
        /// Retrieves the access token from Azure service via end-point of the customer
        /// </summary>
        /// <param name="baseUrl">Base URL of the end-point</param>
        /// <param name="serviceId">Service ID of Azure</param>
        /// <param name="clientId">Client ID of Azure</param>
        /// <param name="clientSecret">Client secret of Azure</param>
        /// <param name="callback">Callback to get the access token</param>
        public static void GetAccessToken(string baseUrl, string serviceId, string clientId, string clientSecret, GetAccessTokenCallback callback)
        {
            string url = string.Format("{0}/identity/accesstoken?serviceId={1}&clientId={2}&secret={3}", baseUrl, serviceId, clientId, clientSecret);
            Script.Literal("console.log('Start retrieving access token NetFiles via URL: ' + {0})", url);
            Script.Literal(
                "fetch({0}).then(response=>response.text()).then(data=>{{callback(data);}}).catch(errorMsg=>{{ console.error('Error occured when retrieving the data from NetFiles');console.error(errorMsg);callback(null);}})",
                url);
        }

        /// <summary>
        /// Get metadata of the NetFiles file by it's File ID
        /// </summary>
        /// <param name="fileUrl">NetFiles file URL</param>
        /// <param name="callback">Callback method when the metadata is retrieved successfully</param>
        public void GetMetadata(string fileUrl, GetMetadataFileCallback callback)
        {
            this.GetFile(fileUrl, callback);
        }

        /// <summary>
        /// Get content of the NetFiles file by it's File ID
        /// </summary>
        /// <param name="fileId">NetFiles file ID</param>
        /// <param name="callback">Callback method when the content is retrieved successfully</param>
        public void GetContent(string fileId, GetFileBase64Callback callback)
        {
            this.GetFileBase64(this.GetContentUri(fileId), callback);
        }

        /// <summary>
        /// Get thumbnail of the NetFiles file by it's File ID
        /// </summary>
        /// <param name="fileId">NetFiles file ID</param>
        /// <param name="callback">Callback method when the thumbnail is retrieved successfully</param>
        public void GetThumbnail(string fileId, GetFileBase64Callback callback)
        {
            this.GetFileBase64(this.GetThumbnailUri(fileId), callback);
        }

        /// <summary>
        /// Get metadata NetFiles file via the NetFiles web service
        /// </summary>
        /// <param name="url">NetFIles file URL</param>
        /// <param name="callback">Callback method when the file is successful retrieved</param>
        private void GetFile(string url, GetMetadataFileCallback callback)
        {
            string netFileUrl = url;
            this.controllerServices.DebugLog("Start retrieve file meta data with url: " + netFileUrl);
            Script.Literal("console.log('Start retrieve file meta data with url: ' + {0} + ' and access token: ' + {1})", netFileUrl, this.accessToken);
            Script.Literal(
                "fetch({0},{{headers:new Headers({{'Authorization':'Bearer '+{1}}})}}).then(data=>{{return Promise.all([data.url,data.json()])}}).then(([url,data])=>{{{2}(data);}}).catch(errorMsg=>{{ console.error('Error occured when retrieving the data from NetFiles');console.error(errorMsg);{2}(null);}})",
                netFileUrl,
                this.accessToken,
                callback);
        }

        /// <summary>
        /// Get content or thumbnail of the NetFiles file via the web service
        /// </summary>
        /// <param name="url">NetFiles file URL</param>
        /// <param name="callback">Callback method when the file content/thumbnail is retrieved successfully</param>
        private void GetFileBase64(string url, GetFileBase64Callback callback)
        {
            string netFileUrl = url;
            this.controllerServices.DebugLog(string.Format("Get File data with URL: {0}", netFileUrl));
            Script.Literal("console.log('Start retrieve file data with url: ' + {0})", netFileUrl);
            Script.Literal(
                "fetch({0},{{headers:new Headers({{'Authorization':'Bearer '+{1}}})}}).then(data=>{{return Promise.all([data.url,data.arrayBuffer()])}}).then(([url,data])=>{{var binary='';var bytes=[].slice.call(new Uint8Array(data));bytes.forEach((b)=>binary+=String.fromCharCode(b));{2}(url.split('/')[4],btoa(binary));}}).catch(errorMsg=>{{ console.error('Error occured when retrieving the data from NetFiles');console.error(errorMsg);{2}(null);}})", 
                netFileUrl, 
                this.accessToken,
                callback);
        }

        /// <summary>
        /// Build content NetFiles File URL
        /// </summary>
        /// <param name="fileUrl">NetFiles file URL</param>
        /// <returns>URL to NetFiles File URL</returns>
        private string GetContentUri(string fileUrl)
        {
            return fileUrl + "/content";
        }

        /// <summary>
        /// Build thumbnail NetFiles File URL
        /// </summary>
        /// <param name="fileUrl">NetFiles file URL</param>
        /// <returns>URL to NetFiles File URL</returns>
        private string GetThumbnailUri(string fileUrl)
        {
            return fileUrl + "/thumbnail";
        }
    }
}
