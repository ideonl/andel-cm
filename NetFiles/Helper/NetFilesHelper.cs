﻿// -----------------------------------------------------------------------
// <copyright file="NetFilesHelper.cs" company="Ideo">
// Copyright (c) Ideo 2021 All Rights Reserved
// </copyright>
// <author>Maarten Brands</author>
// <date>25-09-2021</date>
// ---
namespace Ideo.Andel.ClickMobile.NetFiles.Helper
{
    using System;
    using System.Serialization;
    using Ideo.Andel.ClickMobile.Model;
    using Ideo.Andel.ClickMobile.NetFiles.Model;
    using W6.ClickMobile.Web.AddIns.ClientServices;
    using W6.ClickMobile.Web.Core;
    using W6.ClickMobile.Web.ObjectModel;

    /// <summary>
    /// Helper to store temporary NetFiles files
    /// </summary>
    public class NetFilesHelper
    {
        /// <summary>
        /// Local file storage metadata data
        /// </summary>
        public static string LocalFileStorageMetadata = "metadata_";

        /// <summary>
        /// Local file storage thumbnail data
        /// </summary>
        public static string LocalFileStorageThumbnail = "thumbnail_";

        /// <summary>
        /// Local file storage file data
        /// </summary>
        public static string LocalFileStorageContent = "content_";

        /// <summary>
        /// Local file storage file data
        /// </summary>
        public static string LocalNewFileStorageContent = "new_";

        /// <summary>
        /// Load files to local storage which are stored on the multi-value
        /// </summary>
        /// <param name="controllerServices">Application services</param>
        /// <param name="multivalueOfLinkedBusinessObjectPropertyName">Multi-value property name of the NetFiles files</param>
        /// <param name="formObject">ClickMobile Form object</param>
        /// <param name="propertyNameNetFilesId">Property name of the NetFiles file ID</param>
        /// <param name="accessToken">The access token to authorize to NetFiles</param>
        public static void LoadFilesToLocalStorage(IW6CMControllerServices controllerServices, string multivalueOfLinkedBusinessObjectPropertyName, W6CMObject formObject, string propertyNameNetFilesId, string accessToken)
        {
            W6CMMultiValueOfLinkedBusinessObject list = (W6CMMultiValueOfLinkedBusinessObject)formObject.GetPropertyByName((int)W6CMObjectTypes.Task, multivalueOfLinkedBusinessObjectPropertyName);

            foreach (W6CMLinkedBusinessObjectItem item in list)
            {
                // Get file id from the DisplayString, this should be correctly configured in the Mobility Studio
                string fileId = (string)item.DisplayString;

                NetFilesClient client = new NetFilesClient(controllerServices, accessToken);
                
                client.GetMetadata(
                    fileId, 
                    delegate(NetFile netfile) 
                    {
                       W6CMLocalStorageFactory.GetInstance().SetItem(LocalFileStorageMetadata + netfile.Id, Json.Stringify(netfile));
                        Script.Literal("console.log('Metadata succesfully saved locally: ' + {0})", netfile.Id);
                        controllerServices.DebugLog("Metadata successfully saved locally: " + netfile.Id);
                    });

                client.GetThumbnail(
                    fileId,
                    delegate(string id, string base64)
                    {
                        FileDB filedb = new FileDB();
                        filedb.Key = LocalFileStorageThumbnail + id;
                        filedb.Content = base64;
                        filedb.Reference = formObject.Key;
                        Script.Literal(@"Ideo.Andel.ClickMobile.CustomScripts.IndexDB.PutFile({0}).then(function(result){{console.log(result);}})", filedb);
                    });
                
                client.GetContent(
                    fileId,
                    delegate(string id, string base64)
                    {
                        FileDB filedb = new FileDB();
                        filedb.Key = LocalFileStorageContent + id;
                        filedb.Content = base64;
                        filedb.Reference = formObject.Key;
                        Script.Literal(@"Ideo.Andel.ClickMobile.CustomScripts.IndexDB.PutFile({0}).then(function(result){{console.log(result);}})", filedb);
                    });
            }
        }

        /// <summary>
        /// Delete files from local storage of the given assignment
        /// </summary>
        /// <param name="formObject">ClickMobile Form object</param>
        /// <param name="multivalueName">Multi-Value name of the file list to loop over</param>
        public static void DeleteFilesFromLocalStorage(W6CMObject formObject, string multivalueName)
        {
            W6CMMultiValueOfLinkedBusinessObject list = (W6CMMultiValueOfLinkedBusinessObject)formObject.GetPropertyByName((int)W6CMObjectTypes.Task, multivalueName);

            foreach (W6CMLinkedBusinessObjectItem item in list)
            {
                NetFile netFile = Json.ParseData<NetFile>(W6CMLocalStorageFactory.GetInstance().getItem(LocalFileStorageMetadata + GetFileIdFromURL(item.DisplayString)));

                // Remove files from database
                Script.Literal(
                    @"Ideo.Andel.ClickMobile.CustomScripts.IndexDB.DeleteFile({0}).then(function(result){{console.log(result);}})", 
                    LocalFileStorageThumbnail + netFile.Id);
                Script.Literal(
                    @"Ideo.Andel.ClickMobile.CustomScripts.IndexDB.DeleteFile({0}).then(function(result){{console.log(result);}})", 
                    LocalFileStorageContent + netFile.Id);

                // Remove meta data from the local storage 
                W6CMLocalStorageFactory.GetInstance().removeItem(LocalFileStorageMetadata + netFile.Id);

                Script.Literal("console.log('File deleted: ' + {0})", netFile.Id);
            }
        }

        /// <summary>
        /// Retrieve the NetFiles File ID from the URL
        /// </summary>
        /// <param name="fileUrl">NetFiles URL</param>
        /// <returns>NetFiles File ID</returns>
        public static string GetFileIdFromURL(string fileUrl)
        {
            return fileUrl.Substr(fileUrl.LastIndexOf('/') + 1);
        }
    }
}
