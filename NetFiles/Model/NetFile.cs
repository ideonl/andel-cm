﻿// -----------------------------------------------------------------------
// <copyright file="NetFile.cs" company="Ideo">
// Copyright (c) Ideo 2021 All Rights Reserved
// </copyright>
// <author>Maarten Brands</author>
// <date>25-09-2021</date>
// ---
namespace Ideo.Andel.ClickMobile.NetFiles.Model
{
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// NetFile metadata model
    /// </summary>
    public class NetFile
    {
        /// <summary>
        /// Content type of the file
        /// </summary>
        public string ContentType;

        /// <summary>
        /// URL of the content of the file
        /// </summary>
        public string ContentUrl;

        /// <summary>
        /// NetFile ID
        /// </summary>
        public string Id;

        /// <summary>
        /// URL of the content of the file
        /// </summary>
        public string LocalContentUrl;

        /// <summary>
        /// URL of the content of the file
        /// </summary>
        public string LocalThumbnailUrl;

        /// <summary>
        /// FileName of the file
        /// </summary>
        public string Name;
        
        /// <summary>
        /// Origin system of the NetFile
        /// </summary>
        public string Origin;

        /// <summary>
        /// Reference key of the file
        /// </summary>
        public string ReferenceKey;

        /// <summary>
        /// Tags given to the file
        /// </summary>
        public Dictionary<string, string> Tags;

        /// <summary>
        /// Thumbnail URL of the file
        /// </summary>
        public string ThumbnailUrl;
    }
}
