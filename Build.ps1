﻿Param($SolutionDir = "", $Configuration = "", $ProjectVersion = "");
echo $SolutionDir;
echo $Configuration;
echo $ProjectVersion;
Add-Type -Assembly "System.IO.Compression.FileSystem" ;

####################################################################################################################
# Copy ClickMobile files
####################################################################################################################
function CreateClickMobileZip {
    CreateCleanBuildDir
    $buildOutput = $SolutionDir + "\Bin\" + $Configuration + "\andel-cm.js";
	
    if($Configuration -eq "Debug"){
	    $buildOutput = $SolutionDir + "\Bin\" + $Configuration + "\andel-cm.debug.js";
    }
	
    $ScriptsDir = $SolutionDir + "\Scripts\*";
    $StyleDir = $SolutionDir + "\Styles\*";
    $ImagesDir = $SolutionDir + "\Images\*";

    Copy-Item -Path $buildOutput -Destination $buildDirectory -Force –Recurse
	
    If (Test-Path $ScriptsDir){
		Copy-Item -Path $ScriptsDir -Filter *.js -Destination $buildDirectory -Force –Recurse
	}
	
    If (Test-Path $StyleDir){
		Copy-Item -Path $StyleDir -Filter * -Destination $buildDirectory -Force –Recurse
	}
	
    If (Test-Path $ImagesDir){
		Copy-Item -Path $ImagesDir -Filter * -Destination $buildDirectory -Force –Recurse
	}

    $zipFile = $artifactsDirectory + "\Andel_ClickMobileAddins_" + $ProjectVersion +".zip";
	
    if($Configuration -eq "Debug"){
        $zipFile = $artifactsDirectory + "\Andel_ClickMobileAddins_" + $ProjectVersion +".debug.zip";
    }
	
    # Create zipfile
    [System.IO.Compression.ZipFile]::CreateFromDirectory($buildDirectory, $zipFile);
}

####################################################################################################################
# Create temp directory
####################################################################################################################
function CreateCleanBuildDir {
    If (Test-Path $buildDirectory){
        Remove-Item $buildDirectory -Force -Recurse;
    }
	
    New-Item -ItemType Directory -Force -Path $buildDirectory | out-null;
    
    # Create version file
    $fileContent = "ClickMobile version: " + $ProjectVersion;
    $versionFile = $buildDirectory + "\ClickMobileVersion.txt";
    Set-Content -Value $fileContent -Path $versionFile;
}

####################################################################################################################
# Program execution
####################################################################################################################
$artifactsDirectory = $SolutionDir + "\Artifacts";
$buildDirectory = $SolutionDir + "\ClickMobileBuild";

# Create artifact directory
If (Test-Path $artifactsDirectory){
    Remove-Item $artifactsDirectory -Force -Recurse;
}
New-Item -ItemType Directory -Force -Path $artifactsDirectory | out-null;

CreateClickMobileZip

# Remove temp directory
Remove-Item $buildDirectory -Force -Recurse;

